#### FEATHUR README UPDATED: MARCH 14, 2014

---------------------------------------

Support for Debian 7.x - Fork

---------------------------------------

Feathur is a VPS control panel which is
designed to be used to administrate OpenVZ
and KVM VPS. Feathur supports multiple nodes
using a master and slave type functionality.

Feathur is released under AGPL v3.0. By 
viewing, editing, using or downloading Feathur
(in whole or in part) you agree to abide by
the terms set forth in the license provided.


---------------------------------------

For setup directions and other information about
Feathur. Check out our wiki at:
https://github.com/BlueVM/Feathur/wiki/